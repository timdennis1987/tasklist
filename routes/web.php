<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/task-list', 'TaskListController@taskListView')->name('task-list');
Route::post('/task-list/add', 'TaskListController@addTask');
Route::post('/task-list/complete/{id}', 'TaskListController@completeTask');
Route::post('/task-list/delete/{id}', 'TaskListController@deleteTask');
Route::post('/task-list/filter-completed', 'TaskListController@filterCompletedTasks');
