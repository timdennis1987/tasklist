@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <div class="float-right">
                            <form action="/task-list/filter-completed" method="POST">
                                @csrf

                                <div class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle text-dark" href="#" role="button"
                                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        <span class="caret text-dark"></span>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right shadow" aria-labelledby="navbarDropdown">
                                        <button class="btn text-primary">
                                            @if($showCompleted == 1)
                                                <input type="hidden" name="filter_completed" value="0">
                                                {{ 'Hide Completed' }}
                                            @else
                                                <input type="hidden" name="filter_completed" value="1">
                                                {{ 'Show Completed' }}
                                            @endif
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="card-body">

                        <ul class="list-group">
                            <li class="list-group-item">

                                <form action="/task-list/add" method="POST">
                                    @csrf

                                    <div class="input-group mb-2 mr-sm-2">
                                        <input type="text"
                                               name="name"
                                               class="form-control"
                                               placeholder="Add task...">
                                        <div class="input-group-append">
                                            <button class="btn input-group-text">
                                                <i class="fas fa-plus text-success"></i>
                                            </button>
                                        </div>
                                    </div>

{{--                                    <div class="row">--}}
{{--                                        <div class="col-8">--}}
{{--                                            <input type="text" class="form-control" name="name"--}}
{{--                                                   placeholder="Add task...">--}}
{{--                                        </div>--}}

{{--                                        <div class="mx-auto">--}}
{{--                                            <button class="btn text-success">--}}
{{--                                                Add Task--}}
{{--                                            </button>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

                                </form>

                            </li>
                            @foreach($tasks as $task)
                                <li class="list-group-item {{ $task->completed == 1 ? 'complete' : '' }}">
                                    <div class="float-left">

                                        <form action="/task-list/complete/{{ $task->id }}" method="POST">
                                            @csrf

                                            <button class="btn">
                                                @if($task->completed != 1)
                                                    <input type="hidden" name="completed" value="1">
                                                    <i class="far fa-square"></i>
                                                @else
                                                    <input type="hidden" name="completed" value="0">
                                                    <i class="far fa-check-square text-success"></i>
                                                @endif
                                            </button>

                                            <span class="ml-2">{{ $task->name }}</span>

                                        </form>

                                    </div>
                                    <div class="float-right">

                                        <form action="/task-list/delete/{{ $task->id }}" method="POST">
                                            @csrf

                                            <button class="btn">
                                                <i class="fas fa-times text-danger"></i>
                                            </button>

                                        </form>
                                    </div>
                                </li>
                            @endforeach
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
