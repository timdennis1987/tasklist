<?php

namespace App\Http\Controllers;

use App\Task;
use App\UserSetting;
use Illuminate\Http\Request;

class TaskListController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function createUserSettings()
    {
        $userId = auth()->user()->id;
        $user = UserSetting::where('user_id', $userId)->first();

        if(!$user) {
            UserSetting::create([
                'user_id' => $userId
            ]);
        }
    }

    public function taskListView()
    {
        $this->createUserSettings();

        $userId = auth()->user()->id;
        $user = UserSetting::where('user_id', $userId)->first();
        $showCompleted = UserSetting::where('user_id', $userId)->first();

        $tasks = Task::where('user_id', $userId)
            ->when($user->show_completed == 0, function($query) {
                return $query->where('completed', 0);
            })
            ->orderBy('completed', 'asc')
            ->orderBy('id', 'asc')
            ->get();

        return view('task-list', [
            'tasks' => $tasks,
            'showCompleted' => $showCompleted->show_completed
        ]);
    }

    public function addTask(Request $request)
    {
        Task::create([
            'user_id' => auth()->user()->id,
            'name' => $request->name
        ]);

        return redirect()->back();
    }

    public function completeTask(Request $request, $id)
    {
        $task = Task::where('id', $id)->first();

        $task->update([
            'completed' => $request->completed
        ]);

        return redirect()->back();
    }

    public function deleteTask(Request $request, $id)
    {
        $task = Task::where('id', $id)->first();

        $task->delete();

        return redirect()->back();
    }

    public function filterCompletedTasks(Request $request)
    {
        $userId = auth()->user()->id;
        $user = UserSetting::where('user_id', $userId)->first();

        $user->update([
            'show_completed' => $request->filter_completed
        ]);

        return redirect()->back();
    }
}
